# ----------------------------------------------------
# File: A2part1.py
# ----------------------------------------------------
# Author: Sayyeda Saadia Razvi, Sarah Elias, Jake Lewis, BitBucket handle (SaadiaR)
# ----------------------------------------------------
# Plaftorm: Windows 7
# Environment: Python 2.7 
# Libaries:	NumPy 1.8.1
#			MatPlotLib 1.3.1
#			SciPy 0.14.0
# ----------------------------------------------------
# Description: 
# This code uses object oriented programming to create a triangle class with randomly assigned default vertices.
# The user can choose to animate the triangle as it rotates with a user-defined angular velocity.
# ---------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob

class triangle:
	def __init__(self, sigma=0, prefix='triangle'): 
		self.vertices = self.generatevertices()
		self.sigma = sigma
		self.prefix = prefix
		self.Image_Files = [] 

	# This function generates random vertices that define the triangle 
	def generatevertices(self):
		self.vertex1 = [0,np.random.randint(0,80)]
		self.vertex2 = [np.random.randint(0,80),-np.random.randint(0,80)]
		self.vertex3 = [-np.random.randint(0,80),-np.random.randint(0,80)]
		self.vertices = [self.vertex1, self.vertex2, self.vertex3]
		print "Generating triangle with vertices:", self.vertices
		return self.vertices

	# This function converts a given pixel position in a matrix to a co-ordinate position 
	def get_coordinate(self, pixel, size):
		return[pixel[0]-size/2,size/2-pixel[1]]

	# Generates Triangle from the random vertices by checking if pixels lie within the triangle
	def create_triangle(self):
		image = np.zeros((256,256))
		for yy in range (256):
			for xx in range (256):
				point = np.array(self.get_coordinate([yy,xx],256))
				if self.inside(point):
					image[xx,yy] = 1
		image = ndimage.gaussian_filter(image, self.sigma)
		return image
	
	# Checks if given point in matrix lies within triangle by utilizing cross products
	def inside(self, point):
		inside = True
		for ii in range(len(self.vertices)):
			vector1 = np.array(self.vertices[ii])-np.array(self.vertices[ii-1])
			vector2 = np.array(self.vertices[ii-1])-point
			cross = np.cross(vector1,vector2)
			if cross < 0:
				inside = False
		return inside

	# plots image of triangle
	def show_image(self, graph_name=''):
		im0 = self.create_triangle()
		plt.imshow(im0)
		plt.title(graph_name)
		plt.show()

	# saves 100 images of successively rotated triangles
	def SpinAndSave(self, rate, time = 2, nimages = 100):
		total_rotation = rate * time * 180 / np.pi #how much it rotates [rad/s]
		ii = 0
		image = self.create_triangle()

		for ii in range(nimages):
			angle = total_rotation * (ii / float(nimages)) #angle per spin
			rotated = ndimage.rotate(image, angle, reshape = False, mode='constant', order=1 )

			name = '%s_%03d.png' % (self.prefix, ii)
			misc.imsave(name, rotated)
			

	# creates list of file names starting with triangle prefix
	def GetImageFiles(self):
		name = '%s*.png' % (self.prefix)
		self.Image_Files = glob(name)
		self.Image_Files.sort()
	
	# updates figure with successive rotated images
	def UpdateFig(self, ii):	
		self.im.set_array(misc.imread(self.Image_Files[ii]))
		return self.im,

	# creates animation with series of successively rotated images of the triangle
	def Animate(self):		
		self.GetImageFiles()
		print "We are animating..."
		fig = plt.figure()
		inter = 20 # [ms]
		self.im = plt.imshow((misc.imread(self.Image_Files[0])))
		simulation = animation.FuncAnimation(fig, self.UpdateFig, frames=range(100), interval=inter, blit=False)
		plt.title('Animation')
		plt.show()
		fn = '%s.mp4' % self.prefix
		simulation.save(fn)

def main():

	print "\nThis program creates a random spinning triangle at a user defined RPM within a 256 by 256 window.\n"

	# creates object within class
	tri=triangle(sigma=0, prefix = 'triangle')
	# uses class function to display image of original triangle
	tri.show_image(graph_name='Original Triangle')

	print "\nDo you want to run a simulation of a spinning triangle?"
	
	# while loop to check if user response is valid
	flag = True
	while flag == True:
		user_command = int(input("\nEnter 1 to proceed or 0 to exit: "))
		if (user_command != 0 and user_command != 1):
			print 'Invalid response. Re-enter' 
		else:
			flag = False

	if (user_command == 1):
		print "Angular velocities are usually in range 0 to 10 rad/s."
		rate = int(input("Please enter angular velocity [rad/s]: "))
		tri.SpinAndSave( rate )	
		tri.Animate()

		print "Wheeeeeeee...! Hope you liked the animation!" 
	else: 
		print 'Have a good day!'

main()

