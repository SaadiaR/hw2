# ----------------------------------------------------
# File: A2part2.py
# ----------------------------------------------------
# Author: Sayyeda Saadia Razvi, Sarah Elias, Jake Lewis, BitBucket handle (SaadiaR)
# ----------------------------------------------------
# Plaftorm: Windows 7
# Environment: Python 2.7 
# Libaries:	NumPy 1.8.1
#			MatPlotLib 1.3.1
#			SciPy 0.14.0
# ----------------------------------------------------
# Description: 
# This code uses the images of the rotated triangle created in A2part1.py. 
# It uses binary filters and Harris vertex detection to locate the vertices.
# Using Harris vertex detection it tracks the location of the vertices over time.
# ---------------------------------------------------
# Confluence page: https://wikis.utexas.edu/display/~sar3457/2014-09-30-Image-Processing-Assignment
# ---------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob
import cv2

#Gathers images of triangle into a list
def GetImageFiles(prefix):
	name = '%s*.png' % (prefix)
	Image_Files = glob(name)
	Image_Files.sort()
	return Image_Files

# Converts a given pixel position in a matrix to a co-ordinate position 
def GetCoordinate(pixel, size=256):
	return [pixel[1] - size/2,  size/2 - pixel[0] ]

#Uses binary filters to detect vertices
def GetBinaryVertexImage(image):
	#Preprocessing 
	image = image//255
	image = image > 0 
	closing = ndimage.binary_closing(image)
	opening = ndimage.binary_opening(image)
	Ibinary = ndimage.binary_dilation( closing - opening )
	return Ibinary

#Uses the Harris corner detection theory to detect vertices
def GetHarrisVertexImage(I, kappa=0.15, N=3, wSpace=2, sigma=2, threshold = .8):
	Ix = ndimage.sobel(I, axis=0)
	Iy = ndimage.sobel(I, axis=1)
	gw = np.zeros((N, N))
	center = (N-1)/2
	gw[center,center] = 1
	gw = ndimage.gaussian_filter(gw,sigma)
	Iharris = np.zeros(I.shape)
	for xx in range(center, I.shape[0]-center, wSpace):
		for yy in range(center, I.shape[1]-center, wSpace):
			Ix_window = Ix[xx-center: xx-center+N, yy-center: yy-center+N]
			Ix2 = np.multiply(Ix_window,Ix_window)
			Ix2_avg = np.sum(np.multiply(Ix2,gw))
			Iy_window = Iy[xx-center: xx-center+N, yy-center: yy-center+N]
			Iy2 = np.multiply(Iy_window,Iy_window)
			Iy2_avg = np.sum(np.multiply(Iy2,gw))
			IxIy = np.multiply(Ix_window,Iy_window)
			IxIy_avg = np.sum(np.multiply(IxIy,gw))
			M = [[Ix2_avg,IxIy_avg],[IxIy_avg,Iy2_avg]]
			H = np.linalg.det(M)-kappa*np.trace(M)**2
			Iharris[xx,yy] = H
	Iharris = ndimage.uniform_filter( Iharris, size=wSpace+1, mode="nearest" )
	Id = Iharris > threshold
	return Id

# cv2 Harris function that does not work on my computer but should work on yours
# fails to work on my coputer b/c of the cv2.cvtColor() function
def Harris(image):
	gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
	Harry = cv2.cornerHarris(gray,2,3,0.2)
	Harry = cv2.dilate(Harry,None)
	image[Harry>0.01*Harry.max()] = [0,0,255]
	return image 


#Labels each vertex uniquely so they can be identified
def FindAndLabelVertices(processed_image):
	s2 = ndimage.generate_binary_structure(2,2) 
	labeled_image, num_features = ndimage.label( processed_image, structure=s2 )

# identifying pixels corresponding to vertices
	vertices = []
	for ii in range (num_features):
		vim = (labeled_image == (ii+1))
		vv = ndimage.measurements.center_of_mass(vim)
		vv = GetCoordinate(vv)
		vertices.append(vv)
	return labeled_image, vertices

#prints location of vertices
def PrintVertices(vertices):
	for ii in range (len(vertices)):
		vv = vertices[ii]
		print 'Vertex %s:' % (ii+1), vv

#keeps track of each vertex as it rotates
def Get_Real_Vertex(vertices, last_vertex):
	new_vertices = []
	for vi in (last_vertex):
		vec_len_list = []
		for vj in (vertices):
			len_vec = np.linalg.norm( np.array(vi) - np.array(vj) )
			vec_len_list.append(len_vec)
		index = vec_len_list.index(min(vec_len_list))
		new_vertices.append( vertices[index] )
	return new_vertices


def main():
	#retrieve original image	
	# image = cv2.imread('triangle_000.png').astype(float)
	image = misc.imread('triangle_000.png').astype(float)
	image = np.array(image)	

	bin_image = GetBinaryVertexImage(image)
	# harris_image = Harris(image) # If you want to test the cv2 Harris detector, comment out next line and un-comment this one
	harris_image = GetHarrisVertexImage(image)
	harris_image = ndimage.binary_dilation(harris_image).astype(harris_image.dtype)

	labelled_bin_image, bin_vertices = FindAndLabelVertices( bin_image )
	labelled_harris_image, harris_vertices = FindAndLabelVertices( harris_image )

	# superimposed_bin_image = (image>0) + labelled_bin_image
	# superimposed_harris_image = (image>0) + labelled_harris_image

	print '\nNumber of Binary Vertices:', len(bin_vertices)
	print 'Binary Vertices:'
	PrintVertices(bin_vertices)
	print '\nNumber of Harris Vertices:', len(harris_vertices)
	print 'Harris Vertices:'
	PrintVertices(harris_vertices)
	
	fig1 = plt.figure(figsize=(20,10))
	plt.subplot(131)
	plt.imshow(image)
	plt.title('Original')
	plt.subplot(132)
	# plt.imshow( superimposed_bin_image )
	plt.imshow(labelled_bin_image)
	plt.title("Binary Vertices")
	plt.subplot(133)
	# plt.imshow( superimposed_harris_image )	
	plt.imshow(labelled_harris_image)
	plt.title("Harris Vertices") 
	plt.savefig('Images.png')
	plt.show()


	# Generating Plot of vertices over time using our Harris vertex detector
	Image_Files = GetImageFiles('triangle')

	print '\nGenerating plot of vertex positions in time...'

	vertices0 = harris_vertices
	List_Vertices = [0] * len(Image_Files) 
	List_Vertices[0] = vertices0

	#create list of vertices
	for ii in range (1, len(Image_Files)):
		file_name = Image_Files[ii]
		im = misc.imread(file_name).astype(float)
		# im = misc.imread(file_name).astype(float)
		im = np.array(im)
		harris_im = GetHarrisVertexImage(im)
		labelled_harris_im, harris_vert = FindAndLabelVertices( harris_im )
		List_Vertices[ii] = Get_Real_Vertex(harris_vert,List_Vertices[ii-1])

	x1_list = []
	y1_list = []
	x2_list = []
	y2_list = []
	x3_list = []
	y3_list = []

	#create lists of positions
	for triangle in List_Vertices:
		x1 = triangle[0][0]
		x2 = triangle[1][0]
		x3 = triangle[2][0]
		y1 = triangle[0][1]
		y2 = triangle[1][1]
		y3 = triangle[2][1]
		x1_list.append(x1)
		y1_list.append(y1)
		x2_list.append(x2)
		y2_list.append(y2)
		x3_list.append(x3)
		y3_list.append(y3)

	time_list = np.arange(0, 2, 0.02)

	fig2 = plt.figure(figsize=(20,20))
	plt.subplot(211)
	plt.plot(  time_list, x1_list, time_list, x2_list, time_list, x3_list)
	plt.title('X position vs Time')
	plt.xlabel('Time (s)')
	plt.ylabel('X position')
	plt.subplot(212)
	plt.plot(time_list, y1_list, time_list, y2_list, time_list, y3_list)
	plt.title('Y position vs Time')
	plt.xlabel('Time (s)')
	plt.ylabel('Y position')
	plt.savefig('Trace.png')
	plt.show()


# Our Harris vertex detector works quite well. 
# Our vertex tracking works quite well using the Harris Vertex Detector function that
# we wrote ourselves. However, it times for narrow triangles that may have 2 vertices
# close to each other, it sometimes does not generate great results. This issue is 
# because our Harris Detector not always gives us 3 vertices, but sometimes more. 
# We use a function to only save the vertices that we find to be closest to the last
# set of vertices found. At times, this goes wrong for a set of vertices close to 
# each other.
# Our program could be made more robust with further post processing of the image 
# after applying the Harris vertex detector on it. This way, we will be able to 
# eliminate stray/incorrect vertices found. Additionally, we could further fine tune 
# the parameters sent to the Harris detector function.
# Additionally, we could use the built in openCV2 harris detector for better results.

main()